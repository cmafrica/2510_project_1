// Chelsea Mafrica (cem37)
// CS2510 - project 1 - threads
// Fall 2013

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <setjmp.h>
#include "sem.h"
#include "ult.h"

/******** initialize semaphore ********/
int sem_init(semaphore *sem, int val) 
{
  sem->i = val;
  struct node *node_temp = (struct node *)malloc(sizeof(struct node));
  sem->head = node_temp;
  sem->last = node_temp;
  sem->head->id = -1;
  sem->head->next = NULL;
  sem->head->t = NULL;
  //printf("test val for head node: %d\n",(int) s->head->ptr);
  return 0;
}

/********* get sem value **********/
int sem_getval(semaphore *sem, int *val_loc) {
  *val_loc = sem->i;
  //printf("value: %d\n", s->i);
  return 0;
}

/******* free list in semaphore *****/
int sem_rm(semaphore *sem) {
  struct node *cur = sem->head;
  struct node *next;
  while (cur != NULL) {
    next = cur->next;
    free(cur);
    cur = next;
  }
  return 0;
}

/******* decrement semaphore *******/
void p(semaphore *sem) 
{
  block_sigalrm();

  // decrement sem
  sem->i = (sem->i) - 1;
  
  // if there is more than 1 requesting semaphore
  if (sem->i < -1) {
    // add to queue 
    struct node *n = (struct node *)malloc(sizeof(struct node));
    n->id = current->t_id;
    n->next = NULL;
    sem->last->next = n;
    sem->last = n;
    // set ptr to thread
    sem->last->t = current;
    // update to sem_WAIT
    sem->last->t->t_state = sem_WAIT;
  }


  unblock_sigalrm();
  
  
}

/****** increment semaphore ******/
void v(semaphore *sem) 
{
  block_sigalrm();

  // increment sem
  sem->i = (sem->i) + 1;
  if (sem->head->next != NULL) {
    struct node *temp = sem->head->next;
    // change state of next node to ready
    sem->head->next->t->t_state = lwt_READY;
    // remove node from list
    sem->head->next = sem->head->next->next;
    free(temp);
  }

  unblock_sigalrm();

}
