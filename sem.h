// Chelsea Mafrica (cem37)
// CS2510 - project 1 - threads
// Fall 2013

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <setjmp.h>
#ifndef __semh__
#define __semh__
// semaphore for ULT, including linked list

/***** semaphore structs *******/
// node for linked list
struct node { 
  int id;
  struct node *next;
  struct thread *t;

};

// semaphore 
typedef struct {
  int i;
  struct node *head;
  struct node *last;
} semaphore;

/****** function declarations *******/
int sem_init(semaphore *, int);
int sem_getval(semaphore *, int *);
void p(semaphore *);
void v(semaphore *);
int sem_rm(semaphore *);


#endif //__semh__
