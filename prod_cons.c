// Chelsea Mafrica (cem37)
// CS2510 - project 1 - threads
// Fall 2013

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <setjmp.h>
#include "ult.h"

semaphore produce;
semaphore consume;
semaphore mutex; // to update quantity
int quantity; // current amount to be consumed
int consumed; // total consumed
int produced; // total produced

void consumer()
{
  int id;
  lwt_getid(&id);
  
  printf("enter c%d\n",id);
  lwt_sleep(1);

  // critical section
  p(&consume);
  printf("c%d, quantity: %d\n",id,quantity);
  p(&mutex);
  quantity = quantity - 1;
  consumed = consumed + 1;
  printf("c%d, quantity: %d\n",id,quantity);
  v(&mutex);
  v(&produce);

  printf("exit c%d\n",id);

  lwt_exit(0);
}
void producer() 
{ 
  int id;
  lwt_getid(&id);
  
  printf("enter p%d\n",id);
  lwt_sleep(1);

  // critical section
  p(&produce);
  printf("p%d, quantity: %d\n",id,quantity);
  p(&mutex);
  quantity = quantity + 1;
  produced = produced + 1;
  printf("p%d, quantity: %d\n",id,quantity);
  v(&mutex);
  v(&consume);

  printf("exit p%d\n",id);

  lwt_exit(0);
}
int main (int argc, char *argv[] ) 
{  
   // initialize variables
   quantity = 0;
   produced = 0;
   consumed = 0;

   // initialize semaphores
   printf("Initialize semaphores\n");
   sem_init(&produce,0);
   sem_init(&consume,-1);
   sem_init(&mutex,0);


   // initialize thread subsystem
   printf("Initialize thread subsystem\n");
   lwt_init();

   // create threads - several testers
   printf("Begin creating threads in main\n");
   /************* tester 1 **********
   printf("Using tester 1\n");
   int i;
   for (i = 0; i < 10; i++) {
     printf("Generating more threads in main\n");
     lwt_create(NULL, 0, NULL, producer);
     lwt_usleep(2);
     lwt_create(NULL, 0, NULL, consumer);
     lwt_usleep(2);
   }
   //lwt_create(NULL, 0, NULL, consumer);
   lwt_usleep(1);
   //lwt_create(NULL, 0, NULL, producer);
   lwt_usleep(1);
   /**********************************/

   /************* tester 2 *********/
   printf("Using tester 2\n");
   lwt_create(NULL, 0, NULL, consumer);
   lwt_usleep(1);
   lwt_create(NULL, 0, NULL, consumer);
   lwt_usleep(1);
   lwt_create(NULL, 0, NULL, producer);
   lwt_usleep(1);
   lwt_create(NULL, 0, NULL, producer);
   lwt_usleep(1);
   lwt_create(NULL, 0, NULL, producer);
   lwt_usleep(1);
   /*********************************/

   /************* tester 3 ************
   printf("Using tester 3\n");
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   /************************************/

   /************* tester 4 *********
   printf("Using tester 4\n");
   lwt_create(NULL, 0, NULL, consumer);
   lwt_create(NULL, 0, NULL, consumer);
   /**********************************/

   /************* tester 5 *********
   printf("Using tester 5\n");
   lwt_create(NULL, 0, NULL, producer);
   lwt_create(NULL, 0, NULL, producer);
   /**********************************/

   // exit thread system
   printf("Exit thread subsytem - will wait for all threads\n");
   thread_exit();

   // get rid of semaphore linked list
   printf("Free semaphores - produce \n");
   sem_rm(&produce);
   printf("Free semaphores - consume\n");
   sem_rm(&consume);
   sem_rm(&mutex);

   // statistics
   printf("Final quantity: %d\n",quantity);
   printf("Total produced: %d\n",produced);
   printf("Total consumed: %d\n",consumed);
   printf("Total threads generated: %d\n", t_count);
   printf("End of main\n");

}
