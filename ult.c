// Chelsea Mafrica (cem37)
// CS2510 - project 1 - threads
// Fall 2013

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <setjmp.h>
#include "ult.h"

/************* signal block and unblock ********/
int block_sigalrm() 
{
   sigprocmask(SIG_BLOCK, &sigalrm_mask, NULL);
   return 1;
}
int unblock_sigalrm()
{
   sigprocmask(SIG_UNBLOCK, &sigalrm_mask, NULL);
   ualarm(lwt_quantum, 0);
   return 1;
}
/************* signal handler *************/
void scheduler() 
{
   block_sigalrm();
   int id;
   lwt_getid(&id);
   //printf("scheduler interrupted thread: %d\n", id);

   // if there is 1 thread left and its status is exit, exit
   if (t_existing == 1 && current->t_state == lwt_EXIT) {
      current->t_state == lwt_READY;
      thread_complete = 1;
      return;
   }
   // if there are no other threads, sleep
   if (t_count == 1 || no_active_threads == 1) {
      unblock_sigalrm();
      return;
   }

  // save current thread context
  asm ("movl %%esp, %0" : "=m" (current->sp));
  if (sigsetjmp(current->env, 1) != 0) {
      unblock_sigalrm();
      return;
  }
  else {
      int status = -1;
      // get next thread to execute
    while (1) {
      if (t_existing == 1 && current->t_state == lwt_EXIT) {
        current->t_state == lwt_READY;
        thread_complete = 1;
        siglongjmp(current->env, current->t_id);  
      }
      else if (current->next->t_state == lwt_EXIT && current->next->t_id != 1) {
        struct thread *temp;
        temp = current->next;
        current->next = current->next->next;
        if (current->next->t_id == 1) { 
          t_last = current; 
          //t_last->next = t_initial;
        }
        status = temp->exit_status;
        free(temp->argv);
        free(temp->stack);
        t_existing = t_existing - 1;
        // debug - program may segfault without this, but shouldn't
        if (t_existing != 1 && current->next->t_id!=1) free(temp);
        //int start_id = current->t_id;
        //int cur_id = current->t_id;
        //while (current->
      }
      else if (current->next->t_state == lwt_READY || current->next->t_state == lwt_SLEEP) {
        current = current->next;
        siglongjmp(current->env, current->t_id);  
      }
      current = current->next;
    } 

  }

   // if no other threads were ready, return
   unblock_sigalrm();
}

/************* initialize threads *********/
void lwt_init() 
{

   // itinialize thread count
   t_count = 1;
   t_existing = 1;
   waiting = 0; // no other threads are waiting
   no_active_threads = 1; 
   thread_complete = 0;

   // initialize first thread 
   t_initial = (struct thread *) malloc(sizeof(struct thread));
   t_initial->t_id = t_count;
   t_initial->t_state = lwt_READY;
   t_initial->t_routine = NULL;
   t_initial->argc = 0;
   t_initial->argv = NULL;
   t_initial->stack = malloc(STACK_SIZE);
   t_initial->next = NULL;

   // set current thread to this thread
   current = t_initial;
   t_last = t_initial;
   t_last->next = t_initial;

   // save stack for original process
   asm ("movl %%esp, %0" : "=m" (original_process));
   //printf("esp for original process is %p\n", (void *) original_process);

   // set system stack pointer
   current->sp = current->stack + (STACK_SIZE); 
   //asm ("movl %0, %%esp" : : "m" (current->sp) : "%esp");
   //printf("esp for main thread is %p\n", (void *) current->sp);

   // install sigalarm handler
   sigemptyset(&sigalrm_mask);
   sigaddset(&sigalrm_mask, SIGALRM);
   signal(SIGALRM, scheduler);
   ualarm(lwt_quantum, 0);
 
   return;
}
/*********** extra functions **********/
// get the id of the current thread
int lwt_getid(int *id) {
   *id = current->t_id;
   return 0;
}
// get the state of the current thread
int lwt_getstate(int *state) {
   *state = current->t_state;
   return 0;
}
/******************** create thread *************/
void lwt_create(char *msg, int argc, void *argv, void (*f)())
{
   block_sigalrm();

   // save current thread
   asm ("movl %%esp, %0" : "=m" (current->sp));
   if (sigsetjmp(current->env, 1) != 0) {
      unblock_sigalrm();
      return;
   }
   else {
     // update thread count
     t_count = t_count + 1;
     t_existing = t_existing + 1;
     no_active_threads = 0; 

     // allocate new thread
     struct thread *thr = (struct thread *)malloc(sizeof(struct thread));
     thr->t_id = t_count;
     thr->t_state = lwt_READY;
     thr->t_routine = (*f);
     thr->argc = argc;
     thr->argv = argv;
     thr->stack = malloc(STACK_SIZE);
     thr->next = NULL;   

     // calculate pointer to stack
     thr->sp = thr->stack + (STACK_SIZE);
     //printf("beginning of addr for stack data structure: %p\n", thr->stack);
     //printf("sp for new thread start: %p\n", (int64_t *) thr->sp);
   
     // connect to linked list of threads
     t_last->next = thr;
     t_last = thr;
     t_last->next = t_initial;

     // set to current
     current = thr;

     // update stack pointer
     asm ("movl %0, %%esp" : : "m" (current->sp) : "%esp");

     // reset alarm
     unblock_sigalrm();

     // execute routine
     (*f)();

   }
}
/************* exit thread *********/
// this is called explicitly in a thread
// takes argument status which can be thread id
void lwt_exit(int status) 
{
   block_sigalrm();
   current->exit_status = status;
   current->t_state = lwt_EXIT;
   unblock_sigalrm();
   while((current->t_state)==lwt_EXIT );
}
// this is explicitly called in main to shut down thread system
// waits for all threads to finish before executing
void thread_exit() 
{
   block_sigalrm();
   current->t_state = lwt_EXIT;
   unblock_sigalrm();
   int run = 1;
   while(run==1) {
      if (thread_complete == 1) { 
        block_sigalrm();
        run = 0; 
      }  
   }
   free(t_initial->stack);
   free(t_initial);

}
/************** sleep *************/
// microseconds - takes argument n which specifies microseconds
void lwt_usleep(double n)
{
   block_sigalrm();
   current->t_state = lwt_SLEEP;
   unblock_sigalrm();
   clock_t T0,T1;
   T0 = clock();
   double elapsed;
   n = n/1000000;
   while ((current->t_state)==lwt_SLEEP) {
         T1 = clock();
         elapsed = ((double) (T1 - T0)) / (CLOCKS_PER_SEC);
         //printf("%lu,%f,%f\n",T1,elapsed,n);
         if (elapsed >= n) { 
           block_sigalrm();
           current->t_state = lwt_READY; 
           unblock_sigalrm();
         }
   }
}
// seconds - takes argument n which specifies seconds
void lwt_sleep(long n)
{
   block_sigalrm();
   current->t_state = lwt_SLEEP;
   unblock_sigalrm();
   long T0;
   T0 = time(0);
   while ((current->t_state)==lwt_SLEEP) {
         if ((time(0)-T0) >= n) { 
           block_sigalrm();
           current->t_state = lwt_READY; 
           unblock_sigalrm();
         }
   }
}
/************* thread wait *********/
void thrd_wait(int thr_id) 
{
   //current->t_state = 
}
