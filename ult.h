// Chelsea Mafrica (cem37)
// CS2510 - project 1 - threads
// Fall 2013

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <setjmp.h>
#include "sem.h"

#ifndef __ulth__
#define __ulth__

#define lwt_quantum 10
#define STACK_SIZE (16*1024)

// states
#define lwt_READY 1
#define thrd_WAIT 2
#define sem_WAIT 3
#define lwt_SLEEP 4
#define lwt_EXIT 5

//scheduler
int block_sigalrm();
int unblock_sigalrm();
void scheduler();

// thread primitives
void lwt_init();
void lwt_create(char *msg, int argc, void *argv, void (*f)());
void lwt_exit(int);
void thread_exit();
void thrd_wait();
void lwt_usleep(double);
void lwt_sleep(long);

// extra functions
int lwt_getid(int *);
int lwt_getstate(int *);

// thread structs
struct thread {
   int t_id; // thread id
   int t_state; // thread state
   int exit_status; // status for lwt_exit
   jmp_buf env; // environment
   int64_t sp; // stack pointer
   void (*t_routine)(); // routine to execute
   int argc;
   char *argv;
   void *stack; // pointer to stack (allocated on heap)
   struct thread *next; // next thread

};

int t_count; // count of existing threads
int t_existing;
int no_active_threads;
int waiting;
int thread_complete;
int64_t original_process; // initialized system stack 
struct thread *t_initial; // initial thread, first in linked list (main)
struct thread *t_last; // last thread in linked list
struct thread *current; // currently executing thread

// for sigalarm
sigset_t sigalrm_mask;


#endif //__ulth__
